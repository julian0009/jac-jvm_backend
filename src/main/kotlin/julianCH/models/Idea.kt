package julianCH.models

import javax.persistence.*

@Entity
class Idea(
        @Id @GeneratedValue(strategy = GenerationType.AUTO) @Column(name = "id", updatable = false, nullable = false) val id: Long,
        var name: String,
        var content: String,
        var prio: Int,
        var nameEmployee: String,
        var development: Boolean,
        var discarded: Boolean,
        @Column(name = "latestOption") var option: String
) {
    @OneToMany(mappedBy = "ideas", targetEntity = IdeaSphere::class) var spheres: Set<IdeaSphere>? = null
}
