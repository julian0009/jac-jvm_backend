package julianCH.models

import javax.persistence.*

@Entity
class Backlog(@Id @GeneratedValue(strategy = GenerationType.AUTO) @Column(name = "id", updatable = false, nullable = false) override val id: Long,
              override val nameEmployee: String,
              @OneToOne(cascade = arrayOf(CascadeType.ALL)) @JoinColumn(name = "sphere_id", referencedColumnName = "id") override val ideaSphere: IdeaSphere,
              @OneToOne(mappedBy = "backlog") override val container: IdeaSphere
             ): IdeaSphere(0,nameEmployee, ideaSphere.ideas, ideaSphere,null, null, container)