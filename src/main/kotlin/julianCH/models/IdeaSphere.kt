package julianCH.models

import javax.persistence.*

@Entity
open class IdeaSphere(
        @Id @GeneratedValue(strategy = GenerationType.AUTO) @Column(name = "id", updatable = false, nullable = false) open val id: Long,
        open val nameEmployee: String,
        @ManyToMany
        @JoinTable(
                name = "idInSphere",
                joinColumns = arrayOf(JoinColumn(name = "sphere_id")),
                inverseJoinColumns = arrayOf(JoinColumn(name = "idea_id"))) var ideas: Set<Idea>,
        @OneToOne(cascade = arrayOf(CascadeType.ALL)) @JoinColumn(name = "sphere_id", referencedColumnName = "id") open val ideaSphere: IdeaSphere,
        @OneToOne(cascade = arrayOf(CascadeType.ALL)) @JoinColumn(name = "backlog_id", referencedColumnName = "id", nullable = true) open val backlog: Backlog?,
        @OneToOne(cascade = arrayOf(CascadeType.ALL)) @JoinColumn(name = "nonBacklog_id", referencedColumnName = "id", nullable = true) open val nonBacklog: NonBacklog?,
        @OneToOne(mappedBy = "ideaSphere") open val container: IdeaSphere
        )
