package julianCH.models


import javax.persistence.*

@Entity
class Container(@Id @GeneratedValue(strategy = GenerationType.AUTO) @Column(name = "id", updatable = false, nullable = false) val id: Long,
                val nameEmployee: String,
                @OneToOne(cascade = arrayOf(CascadeType.ALL)) @JoinColumn(name = "backlog_id", referencedColumnName = "id") val backlog: Backlog,
                @OneToOne(cascade = arrayOf(CascadeType.ALL)) @JoinColumn(name = "nonBacklog_id", referencedColumnName = "id") val nonBacklog: NonBacklog
                )