package julianCH.controller

import julianCH.jpaRepositories.ContainerRepository
import julianCH.jpaRepositories.IdeaRepository
import julianCH.models.Container
import julianCH.models.Idea
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*


@RestController
class IdeaController(val ideaRepository: IdeaRepository, containerRepository: ContainerRepository) {
    @PostMapping("/postIdea")
    fun receiveIdea(model: Model, @RequestParam(required = false)idea:Idea) {
        when(idea.option) {
            "add" -> {addIdea(idea);}
            "delete" -> {deleteIdea(idea);}
            "edit" -> {editIdea(idea)};
            "changeStatus" -> {changeIdeaStatus(idea)}}
        }


    @GetMapping("/getAllIdeas")
    fun getAllIdeas(): List<Idea> {
        return ideaRepository.findAll()
    }

    fun addIdea(idea: Idea) {
        ideaRepository.save(idea)
    }

    fun deleteIdea(idea: Idea) {
        ideaRepository.delete(idea)
    }

    fun editIdea(idea: Idea) {
        var ideaN: Idea = ideaRepository.findById(idea.id).get()
        deleteIdea(idea)
        ideaRepository.save(idea)
    }

    fun changeIdeaStatus(idea: Idea) {
        var employee: String = idea.nameEmployee
        var container: Container = containerRepository.getContainerByEmployeeNameAndIdeaId(employee, idea.id)
        //TODO switch from backlog to nonbacklog or vice versa
    }

}