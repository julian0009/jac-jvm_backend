package julianCH.jpaRepositories

import julianCH.models.Backlog
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ArticleRepository : JpaRepository<Backlog, Long>