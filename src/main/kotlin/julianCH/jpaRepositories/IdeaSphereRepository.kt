package julianCH.jpaRepositories

import julianCH.models.IdeaSphere
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface IdeaSphereRepository : JpaRepository<IdeaSphere, Long>