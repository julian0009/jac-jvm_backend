package julianCH.jpaRepositories

import julianCH.models.NonBacklog
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface NonBacklogRepository : JpaRepository<NonBacklog, Long>