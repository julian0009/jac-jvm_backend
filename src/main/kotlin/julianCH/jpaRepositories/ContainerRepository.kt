package julianCH.jpaRepositories

import julianCH.models.Container
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface ContainerRepository : JpaRepository<Container, Long>{
    getContainerByEmployeeNameAndIdeaId(nameEmployee: String, id: Int): Optional<Container>}